Content Moderation Patch

You may see the following error when running drush updb (or using /update.php):

Exception thrown while performing a schema update. SQLSTATE[42000]: Syntax error
or access violation: 1071 Specified key was too long; max key length is 767
bytes: ALTER TABLE {content_moderation_state_field_data} ADD UNIQUE KEY
`content_moderation_state__lookup` (`content_entity_type_id`,
`content_entity_id`, `content_entity_revision_id`, `workflow`, `langcode`);
Array ()

Cause: While the content_moderation core module was still experimental, a
database schema patch was needed for upgrading from 8.3.2 to 8.3.3. It's not
quite enough to update just the database, you need to update Drupal's internal
filter too.

If you're seeing the above error and a series of DB updates are stuck as a
result, or you're unable to edit revisioned content, then chances are it's
because of this. Downloading and installing this module will run the necessary
updates again (it doesn't matter that they're out of sequence.) You can
uninstall and delete it after you've finished.

Please note @Sam152 did all the hard work here, not me.

More details: #2890189: Unofficial content_moderation 8.3.2 to 8.3.3 upgrade path
Related:      #2896630: Unofficial content_moderation 8.3.7 to 8.4.0 upgrade path
